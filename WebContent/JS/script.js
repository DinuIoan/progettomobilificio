$(document).ready(
	function(){
		aggiornaTabella();
		
		window.setInterval(
			function(){
				aggiornaTabella();
				console.log("Tabella aggiornata");
			}	
		, 10000);
		
		$("#cta-inserisciMob").click(
			function(){
				$("#modaleInserimentoMob").modal("show");
			}
		);
		
		$("cta-inserisciCat").click(
			function(){
				$("#modaleInserimentoCat").modal("show");
			}
		);
		
		$("#cta-inserisciMobileModal").click(
			function(){
				let varNomeMobile = $("inputNomeMob").val();
				let varCodiceMobile = $("inputCodiceMobile").val();
				let varDescrizioneMobile = $("inputDescrizioneMobile").val();
				let varPrezzo = $("inputPrezzoMobile").val();
				let valCategorie = [];
				$('#inputCategorieMobile option:selected').each(function(){
					valCategorie.push($(this).val());
				});
				
				$.ajax(
					{
						url: "http://localhost:8081/ProgettoMobilificio/inseriscimobile",
						method: "POST",
						data: {
							inputNomeMobile: varNomeMobile,
							inputCodiceMobile: varCodiceMobile,
							inputDescrizione: varDescrizioneMobile,
							inputPrezzo: varPrezzo,
							inputElencoCategorie: valCategorie
						},
						success: function(rispSuccess){
							switch(rispSuccess.result){
								case "OK":
									aggiornaTabella();
									$("#modaleInserimentoMob").modal("toggle");
									alert("Inserimento avvenuto con successo");
									break;
								case "ERROR":
									alert("ERRORE DI INSERIMENTO " + rispSuccess.description);
									break;
							}
						},
						error: function(risError){
							console.log(risError);
							alert("ERRORE: " + risError.description);
						}
					}
				);
			}
		);
		
		$("#cta-inserisciCategoriaModal").click(
			function(){
				
			}
		);
	}
);

function aggiornaTabella(){
	$.ajax(
		{
			url: "http://localhost:8081/ProgettoMobilificio/recuperamobili",
			method: "POST",
			success: function(rispSuccess){
				stampa(rispSuccess.description);
			},
			error: function(risError){
				console.log("ERRORE");
				alert("ERRORE " + risError.description);
			} 
		}
	);
}

function aggiornaCategoria(){
	$.ajax(
		{
			url: "http://localhost:8081/ProgettoMobilificio/recuperacategorie",
			method: "POST",
			success: function(rispSuccess){
				immettiCategorie(rispSuccess.description);
			},
			error: function(rispError){
				alert("ERRORE " + risError.description);
			}
		}
	);
}

function stampa(elenco){
	let contenuto = "";
	
	for(let i=0; i < elenco.legth; i++){
		contenuto += tabellaRisultato(elenco[i]);
	}
	
	$("#corpoTabella").html(contenuto);
}

function tabellaRisultato(mobile){
	let elencoCat = mobile.elencoCategorie;
	let catObj = "";
	for(let i=0; i < elencoCat.length; i++){
		catObj += elencoCat[i].nomeCategoria + " ";
	}
	
	let ris =	'<tr data-identificatore="' + mobile.codiceMobile + '">';
	ris +=			'<td>' + mobile.nomeMobile + '</td>';
	ris +=			'<td>' + mobile.descrizione.subst(0, 60) + '...</td>';
	ris +=			'<td>' + mobile.prezzo + '</td>';
	ris +=			'<td>' + catObj.trim(); + '</td>';
	ris +=			'<td><button type="button" class="btn btn-danger btn-block" onclick="eliminaMob(this)"><i class="fas fa-trash"></i></button></td>';
	ris +=			'<td><button type="button" class="btn btn-warning btn-block" onclick="modificaMob(this)"><i class="fas fa-pen"></i></button></td>';
}

function immettiCategorie(ris){
	
}