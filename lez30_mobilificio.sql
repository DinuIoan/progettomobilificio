DROP DATABASE IF EXISTS lez30_mobilificio;
CREATE DATABASE lez30_mobilificio;
USE lez30_mobilificio;

CREATE TABLE mobile(
	idMobile INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nomeMob VARCHAR(150) NOT NULL,
    codiceMob VARCHAR(150) NOT NULL UNIQUE,
    descrizione TEXT,
    prezzo FLOAT DEFAULT 0
);

CREATE TABLE categoria(
	idCategoria INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nomeCat VARCHAR(150) UNIQUE,
    codiceCat VARCHAR(150) NOT NULL UNIQUE
);

CREATE TABLE categoriaMobile(
	categoriaId INTEGER NOT NULL,
    mobileId INTEGER NOT NULL,
    PRIMARY KEY(categoriaId,mobileId),
    FOREIGN KEY(categoriaId) REFERENCES categoria(idCategoria) ON DELETE CASCADE,
    FOREIGN KEY(mobileId) REFERENCES mobile(idMobile) ON DELETE CASCADE
);