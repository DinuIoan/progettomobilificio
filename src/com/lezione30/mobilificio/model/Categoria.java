package com.lezione30.mobilificio.model;

import java.sql.Connection;
import java.util.ArrayList;

import com.lezione30.mobilificio.connessione.Connector;

public class Categoria {
	private Integer idCategoria;
	private String nomeCategoria;
	private String codiceCategoria;
	private ArrayList<Mobile> elencoMobili = new ArrayList<Mobile>();
	
	public Categoria(){
		
	}

	public Integer getIdCategoria() {
		return idCategoria;
	}

	public void setIdCategoria(int idCategoria) {
		this.idCategoria = idCategoria;
	}

	public String getNomeCategoria() {
		return nomeCategoria;
	}

	public void setNomeCategoria(String nomeCategoria) {
		this.nomeCategoria = nomeCategoria;
	}

	public String getCodiceCategoria() {
		return codiceCategoria;
	}

	public void setCodiceCategoria(String codiceCategoria) {
		this.codiceCategoria = codiceCategoria;
	}

	public ArrayList<Mobile> getElencoMobili() {
		return elencoMobili;
	}

	public void addMobile(Mobile mobile) {
		this.elencoMobili.add(mobile);
	}
	
	public void removeMobile(Mobile mobile) {
		this.elencoMobili.remove(mobile);
	}
	
}
