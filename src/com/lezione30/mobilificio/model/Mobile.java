package com.lezione30.mobilificio.model;

import java.util.ArrayList;

public class Mobile {
	private Integer idMobile;
	private String nomeMobile;
	private String codiceMobile;
	private String descrizione;
	private Float prezzo;
	private ArrayList<Categoria> elencoCategorie = new ArrayList<Categoria>();
	
	public Mobile(){
		
	}

	public Integer getIdMobile() {
		return idMobile;
	}

	public void setIdMobile(int idMobile) {
		this.idMobile = idMobile;
	}

	public String getNomeMobile() {
		return nomeMobile;
	}

	public void setNomeMobile(String nomeMobile) {
		this.nomeMobile = nomeMobile;
	}

	public String getCodiceMobile() {
		return codiceMobile;
	}

	public void setCodiceMobile(String codiceMobile) {
		this.codiceMobile = codiceMobile;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public Float getPrezzo() {
		return prezzo;
	}

	public void setPrezzo(float prezzo) {
		this.prezzo = prezzo;
	}
	
	public ArrayList<Categoria> getElencoCategorie() {
		return elencoCategorie;
	}

	public void setElencoCategorie(ArrayList<Categoria> elencoCategorie) {
		this.elencoCategorie = elencoCategorie;
	}

	public void addCategoria(Categoria categoria) {
		this.elencoCategorie.add(categoria);
	}
	
	public void removeCategoria(Categoria categoria) {
		this.elencoCategorie.remove(categoria);
	}

	@Override
	public String toString() {
		return "Mobile [idMobile=" + idMobile + ", nomeMobile=" + nomeMobile + ", codiceMobile=" + codiceMobile
				+ ", descrizione=" + descrizione + ", prezzo=" + prezzo + "]";
	}
	
}
