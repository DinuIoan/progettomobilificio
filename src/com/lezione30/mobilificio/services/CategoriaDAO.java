package com.lezione30.mobilificio.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.lezione30.mobilificio.connessione.Connector;
import com.lezione30.mobilificio.model.Categoria;

public class CategoriaDAO implements Dao<Categoria>{

	@Override
	public Categoria getById(int id) throws SQLException {
		Connection conn = Connector.getIstance().getConnection();
		String query="SELECT idCategoria, nomeCat, codiceCat FROM categoria WHERE idCategoria = ?";
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setInt(1, id);
		
		ResultSet result = ps.executeQuery();
		result.next();
		
		Categoria temp = new Categoria();
		temp.setIdCategoria(result.getInt(1));
		temp.setNomeCategoria(result.getString(2));
		temp.setCodiceCategoria(result.getString(3));
		
		return temp;
	}

	@Override
	public ArrayList<Categoria> getAll() throws SQLException {
		ArrayList<Categoria> elencoCategorie = new ArrayList<Categoria>();
		Connection conn = Connector.getIstance().getConnection();
		String query = "SELECT idCategoria, nomeCat, codiceCat FROM categoria";
		PreparedStatement ps = conn.prepareStatement(query);
		
		ResultSet result = ps.executeQuery();
		while(result.next()) {
			Categoria temp = new Categoria();
			temp.setIdCategoria(result.getInt(1));
			temp.setNomeCategoria(result.getString(2));
			temp.setCodiceCategoria(result.getString(3));
			elencoCategorie.add(temp);
		}
		
		return elencoCategorie;
	}

	@Override
	public void insert(Categoria t) throws SQLException {
		Connection conn = Connector.getIstance().getConnection();
		String query ="INSERT INTO categoria (nomeCat, codiceCat) VALUE (?,?)";
		PreparedStatement ps = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, t.getNomeCategoria());
		ps.setString(2, t.getCodiceCategoria());
		
		ps.executeUpdate();
		ResultSet result = ps.getGeneratedKeys();
		result.next();
		
		t.setIdCategoria(result.getInt(1));
	}

	@Override
	public boolean delete(Categoria t) throws SQLException {
		Connection conn = Connector.getIstance().getConnection();
		String query="DELETE FROM categoria WHERE idCategoria = ?";
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setInt(1, t.getIdCategoria());
		
		int deletedRows = ps.executeUpdate();
		if(deletedRows > 0)
			return true;
		else
			return false;
	}

	@Override
	public boolean update(Categoria t) throws SQLException {
		Connection conn = Connector.getIstance().getConnection();
		String query = "UPDATE categoria SET nomeCat = ? WHERE idCategoria = ?";
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setString(1, t.getNomeCategoria());
		ps.setInt(2, t.getIdCategoria());
		
		int updatedRows = ps.executeUpdate();
		if(updatedRows > 0)
			return true;
		else
			return false;
	}

}
