package com.lezione30.mobilificio.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.lezione30.mobilificio.connessione.Connector;
import com.lezione30.mobilificio.model.Mobile;

public class MobileDAO implements Dao<Mobile>{
	
	@Override
	public Mobile getById(int id) throws SQLException {
		Connection conn = Connector.getIstance().getConnection();
		String query = "SELECT idMobile, nomeMob, codiceMob, decrizione, prezzo FROM mobile WHERE idMobile = ?";
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setInt(1, id);
		
		ResultSet result = ps.executeQuery();
		result.next();
		
		Mobile temp = new Mobile();
		temp.setIdMobile(result.getInt(1));
		temp.setNomeMobile(result.getString(2));
		temp.setCodiceMobile(result.getString(3));
		temp.setDescrizione(result.getString(4));
		temp.setPrezzo(result.getFloat(5));
		
		return temp;
	}

	@Override
	public ArrayList<Mobile> getAll() throws SQLException {
		ArrayList<Mobile> elencoMobili = new ArrayList<Mobile>();
		Connection conn = Connector.getIstance().getConnection();
		String query = "SELECT idMobile, nomeMob, codiceMob, descrizione, prezzo FROM mobile";
		PreparedStatement ps = conn.prepareStatement(query);
		
		ResultSet result = ps.executeQuery();
		while(result.next()) {
			Mobile temp = new Mobile();
			temp.setIdMobile(result.getInt(1));
			temp.setNomeMobile(result.getString(2));
			temp.setCodiceMobile(result.getString(3));
			temp.setDescrizione(result.getString(4));
			temp.setPrezzo(result.getFloat(5));
			elencoMobili.add(temp);
		}
		
		return elencoMobili;
	}

	@Override
	public void insert(Mobile t) throws SQLException {
		Connection conn = Connector.getIstance().getConnection();
		String query = "INSERT INTO mobile (nomeMob, codiceMob, descrizione, prezzo) VALUE (?,?,?,?)";
		PreparedStatement ps = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, t.getNomeMobile());
		ps.setString(2, t.getCodiceMobile());
		ps.setString(3, t.getDescrizione());
		ps.setFloat(4, t.getPrezzo());
		
		ps.executeUpdate();
		ResultSet result = ps.getGeneratedKeys();
		result.next();
		
		t.setIdMobile(result.getInt(1));
	}

	@Override
	public boolean delete(Mobile t) throws SQLException {
		Connection conn = Connector.getIstance().getConnection();
		String query = "DELETE FROM articolo WHERE idMobile = ?";
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setInt(1, t.getIdMobile());
		
		int deletedRows = ps.executeUpdate();
		if(deletedRows > 0)
			return true;
		else
			return false;
	}

	@Override
	public boolean update(Mobile t) throws SQLException {
		Connection conn = Connector.getIstance().getConnection();
		String query = "UPDATE mobile SET nomeMob = ?, descrizione = ?, prezzo = ? WHERE idMobile = ?";
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setString(1, t.getNomeMobile());
		ps.setString(2, t.getDescrizione());
		ps.setFloat(3, t.getPrezzo());
		ps.setInt(4, t.getIdMobile());
		
		int updatedRows = ps.executeUpdate();
		if(updatedRows > 0)
			return true;
		else
			return false;
	}

}
