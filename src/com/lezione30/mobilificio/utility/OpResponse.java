package com.lezione30.mobilificio.utility;

public class OpResponse {
	private String result;
	private String description;
	
	public OpResponse(String result, String description) {
		super();
		this.result = result;
		this.description = description;
	}
}
