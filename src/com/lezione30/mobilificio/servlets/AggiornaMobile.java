package com.lezione30.mobilificio.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.lezione30.mobilificio.model.Mobile;
import com.lezione30.mobilificio.services.MobileDAO;
import com.lezione30.mobilificio.utility.OpResponse;

@WebServlet("/aggiornamobile")
public class AggiornaMobile extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
    public AggiornaMobile() {
        super();
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String codiceMobile = request.getParameter("codiceMobile") != null ? request.getParameter("codiceMobile") : "";
		String nomeMobile = request.getParameter("nomeMobile") != null ? request.getParameter("nomeMobile") : "";
		String descizioneMobile = request.getParameter("descrizioneMobile") != null ? request.getParameter("descrizioneMobile") : "";
		float prezzo = request.getParameter("prezzoMobile") != null ? Float.parseFloat(request.getParameter("prezzoMobile")) : -1;
		
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		MobileDAO mobDao = new MobileDAO();
		
		if(codiceMobile.isEmpty() || codiceMobile.trim().equals("") || nomeMobile.isEmpty() || nomeMobile.trim().equals("") || prezzo == -1 || descizioneMobile.isEmpty() || descizioneMobile.trim().equals("")) {
			out.print(new Gson().toJson(new OpResponse("ERRORE", "Errore nell'inserimento dei valori!")));
			return;
		}
		
		try {
			ArrayList<Mobile> elenco = mobDao.getAll();
			for(int i=0; i < elenco.size(); i++) {
				if(elenco.get(i).getCodiceMobile().equalsIgnoreCase(codiceMobile)) {
					Mobile temp = elenco.get(i);
					temp.setNomeMobile(nomeMobile);
					temp.setDescrizione(descizioneMobile);
					temp.setPrezzo(prezzo);
					
					if(mobDao.update(temp)) {
						out.print(new Gson().toJson(new OpResponse("OK", temp.toString())));
						return;
					}
					
					out.print(new Gson().toJson(new OpResponse("ERRORE", "Modifica non effettuata!")));
					return;
				}
			}
			out.print(new Gson().toJson(new OpResponse("ERRORE", "Articolo non trovato!")));
		}catch(SQLException ex) {
			out.print(new Gson().toJson(new OpResponse("ERRORE", ex.getMessage())));
		}
	}

}
