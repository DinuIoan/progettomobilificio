package com.lezione30.mobilificio.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.lezione30.mobilificio.model.Mobile;
import com.lezione30.mobilificio.services.MobileDAO;
import com.lezione30.mobilificio.utility.OpResponse;

@WebServlet("/recuperamobili")
public class RecuperaMobili extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public RecuperaMobili() {
        super();
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		MobileDAO mobDao = new MobileDAO();
		
		String codMob = request.getParameter("codMobile") != null ? request.getParameter("codMobile") : "";
		
		try {
			if(!codMob.isBlank() && !codMob.isEmpty() && !codMob.equalsIgnoreCase("")) {
				ArrayList<Mobile> elenco = mobDao.getAll();
				for(int i = 0; i < elenco.size(); i++) {
					if(elenco.get(i).getCodiceMobile().equalsIgnoreCase(codMob)) {
						String elementoJson = new Gson().toJson(elenco.get(i));
						out.print(new Gson().toJson(new OpResponse("OK", elementoJson)));
					}
				}
			}
			else {
				ArrayList<Mobile> elenco = mobDao.getAll();
				String elencoJson = new Gson().toJson(elenco);
				out.print(new Gson().toJson(new OpResponse("OK", elencoJson)));
			}
		}catch(Exception ex) {
			out.print(new Gson().toJson(new OpResponse("ERRORE", ex.getMessage())));
			System.out.println(ex.getMessage());
		}
	}

}
