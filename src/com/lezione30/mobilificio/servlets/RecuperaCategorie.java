package com.lezione30.mobilificio.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.lezione30.mobilificio.model.Categoria;
import com.lezione30.mobilificio.services.CategoriaDAO;
import com.lezione30.mobilificio.utility.OpResponse;


@WebServlet("/recuperacategorie")
public class RecuperaCategorie extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public RecuperaCategorie() {
        super();
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		CategoriaDAO catDao = new CategoriaDAO();
		String codCategoria = request.getParameter("codiceCategoria") != null ? request.getParameter("codiceCategoria") : "";
		
		try {
			if(!codCategoria.isBlank() && !codCategoria.isEmpty() && !codCategoria.equalsIgnoreCase("")) {
				ArrayList<Categoria> elenco = catDao.getAll();
				for(int i=0; i < elenco.size(); i++) {
					if(elenco.get(i).getCodiceCategoria().equalsIgnoreCase(codCategoria)) {
						String elementoJson = new Gson().toJson(elenco.get(i));
						out.print(new Gson().toJson(new OpResponse("OK", elementoJson)));
					}
				}
			}
			else {
				ArrayList<Categoria> elenco = catDao.getAll();
				String elementoJson = new Gson().toJson(elenco);
				out.print(new Gson().toJson(new OpResponse("OK", elementoJson)));
			}
		}catch(Exception ex) {
			out.print(new Gson().toJson(new OpResponse("ERROR", ex.getMessage())));
			System.out.println(ex.getMessage());
		}
	}

}
