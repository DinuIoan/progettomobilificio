package com.lezione30.mobilificio.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.lezione30.mobilificio.model.Mobile;
import com.lezione30.mobilificio.services.MobileDAO;
import com.lezione30.mobilificio.utility.OpResponse;

@WebServlet("/rimuovimobile")
public class RimuoviMobile extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public RimuoviMobile() {
        super();
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		String codiceMobile = request.getParameter("inputCodiceModifica") != null ? request.getParameter("inputCodiceModifica") : "";
		
		if(!codiceMobile.isBlank() && !codiceMobile.isEmpty() && !codiceMobile.equalsIgnoreCase("")) {
			MobileDAO mobDao = new MobileDAO();
			try {
				ArrayList<Mobile> elenco = mobDao.getAll();
				for(int i=0; i < elenco.size(); i++) {
					if(elenco.get(i).getCodiceMobile().equalsIgnoreCase(codiceMobile)) {
						Mobile temp = mobDao.getById(elenco.get(i).getIdMobile());
						if(mobDao.delete(temp)) {
							String elemento = new Gson().toJson(elenco.get(i));
							out.print(new Gson().toJson(new OpResponse("OK", elemento)));
						}
						else {
							out.print(new Gson().toJson(new OpResponse("ERRORE", "Eliminazione non riuscita!")));
						}
					}
				}
			}catch(SQLException ex) {
				out.print(new Gson().toJson(new OpResponse("ERRORE", ex.getMessage())));
			}
		}
	}

}
