package com.lezione30.mobilificio.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.lezione30.mobilificio.model.Categoria;
import com.lezione30.mobilificio.model.Mobile;
import com.lezione30.mobilificio.services.CategoriaDAO;
import com.lezione30.mobilificio.services.MobileDAO;
import com.lezione30.mobilificio.utility.OpResponse;

@WebServlet("/inseriscimobile")
public class InserisciMobile extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public InserisciMobile() {
        super();
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nomeMob = request.getParameter("inputNomeMobile");
		String descrizione = request.getParameter("inputDescrizione");
		Float prezzoMobile = Float.parseFloat(request.getParameter("inputPrezzo"));
		String codiceMobile = request.getParameter("inputCodiceMobile");
		String[] elencoCategorie = new Gson().fromJson(request.getParameter("inputElencoCategorie"), String[].class);
		PrintWriter out = response.getWriter();
		
		Mobile temp = new Mobile();
		temp.setCodiceMobile(codiceMobile);
		temp.setNomeMobile(nomeMob);
		temp.setDescrizione(descrizione);
		temp.setPrezzo(prezzoMobile);
		
		for(int i = 0; i < elencoCategorie.length; i++) {
			CategoriaDAO catDao = new CategoriaDAO();
			try {
				ArrayList<Categoria> elencoCatTotali = catDao.getAll();
				for(int k = 0; k < elencoCatTotali.size(); k++) {
					if(elencoCatTotali.get(k).getNomeCategoria().equalsIgnoreCase(elencoCategorie[i])) {
						temp.addCategoria(elencoCatTotali.get(k));
					}
				}
			} catch (SQLException e) {
				out.print(new Gson().toJson(new OpResponse("ERROR", e.getMessage())));
				return;
			}
		}
		
		MobileDAO mobDao = new MobileDAO();
		String err = "";
		
		try {
			mobDao.insert(temp);
		}catch(SQLException ex) {
			err = ex.getMessage();
		}
		
		Gson jsonizzatore = new Gson();
		
		if(temp.getIdMobile() != null) {
			out.print(jsonizzatore.toJson(new OpResponse("OK", "")));
		}
		else {
			out.print(new Gson().toJson(new OpResponse("ERRORE", err)));
		}
	}

}
