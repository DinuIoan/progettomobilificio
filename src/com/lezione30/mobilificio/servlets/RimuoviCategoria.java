package com.lezione30.mobilificio.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.lezione30.mobilificio.model.Categoria;
import com.lezione30.mobilificio.services.CategoriaDAO;
import com.lezione30.mobilificio.utility.OpResponse;

@WebServlet("/rimuovicategoria")
public class RimuoviCategoria extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
    public RimuoviCategoria() {
        super();
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		String codiceCategoria = request.getParameter("codiceCategoria") != null ? request.getParameter("codiceCategoria") : "";
		
		if(!codiceCategoria.trim().equals("") && !codiceCategoria.isEmpty()) {
			CategoriaDAO catDao = new CategoriaDAO();
			try {
				ArrayList<Categoria> elenco = catDao.getAll();
				for(int i = 0; i < elenco.size(); i++) {
					if(elenco.get(i).getCodiceCategoria().equalsIgnoreCase(codiceCategoria)) {
						Categoria temp = elenco.get(i);
						if(catDao.delete(temp)) {
							out.print(new Gson().toJson(new OpResponse("OK", "Elemento eliminato con successo.")));
							return;
						}
						else {
							out.print(new Gson().toJson(new OpResponse("ERROR", "Elemento non eliminato!")));
							return;
						}
					}
				}
				out.print(new Gson().toJson(new OpResponse("ERROR", "Elemento da eliminare non trovato")));
			}catch(SQLException ex) {
				out.print(new Gson().toJson(new OpResponse("ERROR", ex.getMessage())));
			}
		}
	}

}
