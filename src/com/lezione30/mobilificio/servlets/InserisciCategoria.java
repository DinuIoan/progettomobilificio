package com.lezione30.mobilificio.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.lezione30.mobilificio.model.Categoria;
import com.lezione30.mobilificio.services.CategoriaDAO;
import com.lezione30.mobilificio.utility.OpResponse;

@WebServlet("/inseriscicategoria")
public class InserisciCategoria extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
    public InserisciCategoria() {
        super();
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nomeCategoria = request.getParameter("nomeCategoria") != null ? request.getParameter("nomeCategoria") : "";
		String codiceCategoria = request.getParameter("codiceCategoria") != null ? request.getParameter("codiceCategoria") : "";
		
		Categoria temp = new Categoria();
		temp.setNomeCategoria(nomeCategoria);
		temp.setCodiceCategoria(codiceCategoria);
		
		CategoriaDAO catDao = new CategoriaDAO();
		String err = "";
		
		try {
			catDao.insert(temp);
		}catch(SQLException ex) {
			err = ex.getMessage();
		}
		
		PrintWriter out = response.getWriter();
		if(temp.getIdCategoria() != null) {
			out.print(new Gson().toJson(new OpResponse("OK", "Categoria creata con successo")));
		}
		else {
			out.print(new Gson().toJson(new OpResponse("ERROR", err)));
		}
	}

}
