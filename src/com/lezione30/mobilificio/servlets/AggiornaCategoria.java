package com.lezione30.mobilificio.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.lezione30.mobilificio.model.Categoria;
import com.lezione30.mobilificio.services.CategoriaDAO;
import com.lezione30.mobilificio.utility.OpResponse;

@WebServlet("/aggiornacategoria")
public class AggiornaCategoria extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
    public AggiornaCategoria() {
        super();
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nomeCategoria = request.getParameter("nomeCategoria") != null ? request.getParameter("nomeCategoria") : "";
		String codiceCategoria = request.getParameter("codiceCategoria") != null ? request.getParameter("codiceCategoria") : "";
		
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		CategoriaDAO catDao = new CategoriaDAO();
		
		if(codiceCategoria.trim().equals("") || codiceCategoria.isEmpty() || !nomeCategoria.trim().equals("") || !nomeCategoria.isEmpty()) {
			out.print(new Gson().toJson(new OpResponse("ERROR", "Errore nella immissione dei dati.")));
			return;
		}
		
		try {
			ArrayList<Categoria> elenco = catDao.getAll();
			for(int i = 0; i < elenco.size(); i++){
				if(elenco.get(i).getCodiceCategoria().equalsIgnoreCase(codiceCategoria)) {
					Categoria temp = elenco.get(i);
					temp.setNomeCategoria(nomeCategoria);
					
					if(catDao.update(temp)) {
						out.print(new Gson().toJson(new OpResponse("OK", "Elemento modificato con successo.")));
						return;
					}
					else {
						out.print(new Gson().toJson(new OpResponse("ERROR", "Elemento non modificato.")));
						return;
					}
				}
			}
			out.print(new Gson().toJson(new OpResponse("ERRORE", "Articolo non trovato!")));
		}catch(SQLException ex) {
			out.print(new Gson().toJson(new OpResponse("ERRORE", ex.getMessage())));
		}
	}

}
